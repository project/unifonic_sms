<?php

namespace Drupal\unifonic_sms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements Unifonic API configuration.
 *
 * Creates the administrative form with settings used to
 * connect with Unifonic API.
 *
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unifonic_sms.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unifonic_sms.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unifonic_sms.settings');

    // @see https://api.unifonic.com/rest/Messages/Send
    $form['unifonic_sms'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Unifonic Messages'),
      '#open' => TRUE,
    ];

    $form['unifonic_sms']['AppSid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AppSid'),
      '#description' => $this->t('You will find your AppSid in "Dev Tools" after you login to Unifonic Digital Platform'),
      '#default_value' => $config->get('AppSid'),
      '#required' => TRUE,
    ];

    $form['unifonic_sms']['endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Unifonic Messages Endpoint'),
      '#description' => $this->t('Unifonic Messaging endpoint.'),
      '#default_value' => $config->get('endpoint'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('unifonic_sms.settings');
    $config
      ->set('AppSid', $form_state->getValue('AppSid'))
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
