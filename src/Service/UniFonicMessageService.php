<?php

namespace Drupal\unifonic_sms\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;

/**
 * Creates an interface to SMS Messages to mobile SMS using Unifonic.
 *
 * @see https://api.unifonic.com/rest/Messages/Send
 */
class UniFonicMessageService {

  private ConfigFactoryInterface $configFactory;
  private Client $client;
  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Your AppSid Key.
   *
   * @var string
   */
  private $appsid;

  /**
   * Unifonic Messages's endpoint service.
   *
   * @var string
   */
  private $unifonicEndpoint;

  /**
   * Priority of each SMS Messages.
   *
   * @var string
   */

  /**
   * Priority of each push notification.
   *
   * @var string
   */
  private $priority = 'high';

  /**
   * Class contructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The http_client.
   * @param \GuzzleHttp\Client $client
   *   The http_client.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The http_client.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Client $client, LoggerChannelFactoryInterface $loggerFactory) {
    $this->configFactory = $configFactory;
    $config = $this->configFactory->get('unifonic_sms.settings');
    $this->appsid = $config->get('AppSid');
    $this->unifonicEndpoint = $config->get('endpoint');
    $this->client = $client;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Execute the push notification.
   *
   * @param array $param
   *   Parameters for payload.
   *
   * @return object
   *   Unifonic's Messassing response.
   */
  private function sendPushNotification(array $param) {
    // Build the header of our request to Unifonic.
    // The header is composed by Content-Type.
    $headers = $this->buildHeader();

    // Build the body of our request.
    // The body is composed by an array of data.
    if (!$body = $this->buildMessage($param)) {
      return FALSE;
    }

    $opts = [
      'body'    => $body,
      'headers' => $headers,
    ];

    $request = $this->client->request('POST', $this->unifonicEndpoint, $opts);
    $response = $request->getBody()->getContents();

    return $response;
  }

  /**
   * Builds request header.
   */
  private function buildHeader() {
    return [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];
  }

  /**
   * Builds request body.
   *
   * @param array $param
   *   Data for payload.
   *
   * @return string
   *   Prepared payload for SMS.
   */
  private function buildMessage(array $param) {
    // Okay if we have at least the AppSid,recipient and body.
    $message = $this->addMandatoryFields($param);

    return $message;
  }

  /**
   * Adds mandatory fields to payload.
   *
   * @return string
   *   Mandatory payload.
   */
  private function addMandatoryFields($param) {
    // This is the core notification body.
    $message['AppSid'] = $this->appsid;

    // Add them to notification body.
    if (isset($param['recipient'])) {
      $message['Recipient'] = $param['recipient'];
    }

    // Add them to notification body.
    if (isset($param['body'])) {
      $message['Body'] = $param['body'];
    }

    // Add them to notification body.
    if (isset($param['senderID'])) {
      $message['SenderID'] = $param['senderID'];
    }

    $message['priority'] = $this->priority;

    if (isset($param['priority'])) {
      $message['priority'] = $param['priority'];
    }

    if (isset($param['timeScheduled'])) {
      $message['TimeScheduled'] = $param['timeScheduled'];
    }

    return http_build_query($message);
  }

  /**
   * Sends SMS.
   *
   * @param array $param
   *   Parameters for payload. Expected values are:
   *   - $param['AppSid']
   *     A character string that uniquely identifies your app,
   *     you will find your AppSid
   *     in "Dev Tools" after you login to Unifonic Digital Platform
   *   - $param['body']
   *     Message body supports both English and unicodes characters,
   *     concatenated messages is supported
   *   - $param['Recipient']
   *     Destination mobile number, mobile numbers must be
   *     in international format without 00 or + Example: (4452023498)
   *   Optional values are:
   *   - $param['SenderID']
   *     The SenderID to send from, App default SenderID
   *      is used unless else stated
   *   - $param['Priority']
   *     Send high priority messages, possible value is " High " only.
   *     High priority sending is available for advanced plans
   *     and through balance
   *   - $param['TimeScheduled']
   *     Schedule send messages,
   *     in the following format yyyy-mm-dd HH:mm:ss
   *     Note, The scheduled messages in the past will be sent directly.
   *
   * @return array
   *   return array of response
   */
  public function send(array $param) {
    // We absolutely need the params. If it was not provided, return early.
    if (empty($param)) {
      // Get the logger channel (e.g., 'my_module')
      $logger = $this->loggerFactory->get('unifonic_sms');

      // Now call notice() on the LoggerChannel
      $logger->notice('Failure message: @error', [
        '@error' => 'parameters are not set',
      ]);

      return FALSE;
    }

    $response = $this->sendPushNotification($param);

    $responseBody = json_decode($response);
    $errorMessage = " ";
    switch ($responseBody->errorCode) {
      case 'ER-00':
        return $responseBody;

      case 'ER-01':
        $errorMessage = 'Invalid AppSid';
        break;

      case 'ER-02':
        $errorMessage = 'Missing parameter';
        break;

      case 'ER-03':
        $errorMessage = 'Sender ID already exists';
        break;

      case 'ER-04':
        $errorMessage = 'Wrong sender ID format, sender ID should not exceed 11 characters or 16 numbers, only English letters allowed with no special characters or spaces';
        break;

      case 'ER-05':
        $errorMessage = 'Sender ID is blocked and can’t be used';
        break;

      case 'ER-06':
        $errorMessage = 'Sender ID doesnt exist';
        break;

      case 'ER-07':
        $errorMessage = 'Default sender ID cant be deleted';
        break;

      case 'ER-08':
        $errorMessage = 'Sender ID is not approved';
        break;

      case 'ER-09':
        $errorMessage = 'No sufficient balance';
        break;

      case 'ER-10':
        $errorMessage = 'Wrong number format , mobile numbers must be in international format without 00 or + Example: (4452023498)';
        break;

      case 'ER-11':
        $errorMessage = 'Unsupported destination';
        break;

      case 'ER-12':
        $errorMessage = 'Message body exceeded limit';
        break;

      case 'ER-13':
        $errorMessage = 'Service not found';
        break;

      case 'ER-14':
        $errorMessage = 'Sender ID is blocked on the required destination';
        break;

      case 'ER-15':
        $errorMessage = 'User is Not active';
        break;

      case 'ER-16':
        $errorMessage = 'Exceed throughput limit';
        break;

      case 'ER-17':
        $errorMessage = 'Inappropriate content in message body';
        break;

      case 'ER-18':
        $errorMessage = 'Invalid Message ID';
        break;

      case 'ER-19':
        $errorMessage = 'Wrong date format, date format should be yyyy-mm-dd';
        break;

      case 'ER-20':
        $errorMessage = 'Page limit Exceeds (10000)';
        break;

      case 'ER-21':
        $errorMessage = 'Method not found.';
        break;

      case 'ER-22':
        $errorMessage = 'Language not supported';
        break;
    }

    // Something went wrong. We didn't sent SMS.
    // Common errors:
    // - Body cannot be blank.
    // - Recipient cannot be blank.
    // @see https://unifonic.docs.apiary.io/#reference/errors-list/send

    $logger = $this->loggerFactory->get('unifonic_sms');
    // Now call notice() on the LoggerChannel
    $logger->notice('Failure message: @error', [
      '@error' => $errorMessage,
    ]);

    return FALSE;
  }

}
