# Unifonic Messaging

INTRODUCTION
------------

Provides a simple way to connect with Unifonic API.

Not all possible Unifonic parameters are acceptable yet.
The common ones are implemented.

* body
* recipient
* senderID
* priority
* timeScheduled

INSTALLATION
------------

Install as usual, see
 <https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules>
 for more information.

CONFIGURATION
-------------

The module has a simple configuration screen,
where you provide AppSid and messages endpoint.

* In order to send the push notification:

```php
    $posts = \Drupal::service('unifonic_sms.messages')->send([
      'body' => 'xxxxx',
      'recipient' => '962795051547',
      'senderID' => 'xxx',
      'priority' => 'high',
      'timeScheduled' => '2020-01-09 05:35:01'
    ]);
```

MAINTAINERS
-----------

Current maintainers:

* Ahmad Halah - https://www.drupal.org/u/ahmadhalah
